package com.example.timesheetapp;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.TimeSheetViewHolder> {
    private List<TimeSheet> projectList;
    public ListAdapter(List<TimeSheet> projectList) {
        this.projectList = projectList;
    }
    @NonNull
    @Override
    public ListAdapter.TimeSheetViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.project_list, parent, false);
        return new TimeSheetViewHolder(view);    }

    @Override
    public void onBindViewHolder(@NonNull ListAdapter.TimeSheetViewHolder holder, int position) {
        position = holder.getAdapterPosition();
        TimeSheet timeSheet = projectList.get(position);
        holder.list_projectName.setText(timeSheet.getProjectName());
        holder.list_projectTask.setText(timeSheet.getTaskName());
        holder.list_projectAssignee.setText(timeSheet.getAssignedTo());
        holder.list_projectDateFrom.setText(timeSheet.getFrom());
        holder.list_projectDateTo.setText(timeSheet.getTo());
        holder.list_projectStatus.setText(timeSheet.getStatus());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopUp(v, holder.getAdapterPosition());
            }
        });

    }

    private void showPopUp(View v, int position) {
        TimeSheet timeSheet = projectList.get(position);
        PopupMenu popupMenu = new PopupMenu(v.getContext(), v);
        popupMenu.getMenuInflater().inflate(R.menu.menu, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.edit:
                    Intent i = new Intent(v.getContext(), CreateFormActivity.class);
                    i.putExtra("projectid", timeSheet.getProjectId());
                    v.getContext().startActivity(i);
                    break;
                case R.id.delete:
                    deleteItem(timeSheet.getProjectId(), position, v.getContext());
                    break;
            }
            return false;
        });
        popupMenu.show();
    }
    private void deleteItem(int projectId, int position, Context context) {
        class deleteAll extends AsyncTask<Void, Void, List<TimeSheet>> {
            @Override
            protected List<TimeSheet> doInBackground(Void... voids) {
                DatabaseClient.getInstance(context)
                        .getAppDatabase()
                        .timeSheetDAO()
                        .deleteProjectFromId(projectId);

                return projectList;
            }

            @Override
            protected void onPostExecute(List<TimeSheet> tasks) {
                super.onPostExecute(tasks);
                projectList.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, projectList.size());            }
        }
        deleteAll savedTasks = new deleteAll();
        savedTasks.execute();
    }

    @Override
    public int getItemCount() {
        return projectList.size();
    }

    public class TimeSheetViewHolder extends RecyclerView.ViewHolder {
        public TextView
                list_projectName,
                list_projectTask,
                list_projectAssignee,
                list_projectDateFrom,
                list_projectDateTo,
                list_projectStatus;
        public TimeSheetViewHolder(@NonNull View itemView) {
            super(itemView);

            list_projectName = itemView.findViewById(R.id.list_projectName);
            list_projectTask = itemView.findViewById(R.id.list_projectTask);
            list_projectAssignee = itemView.findViewById(R.id.list_projectAssignee);
            list_projectDateFrom = itemView.findViewById(R.id.list_projectDateFrom);
            list_projectDateTo = itemView.findViewById(R.id.list_projectDateTo);
            list_projectStatus = itemView.findViewById(R.id.list_projectStatus);

        }
    }
}
