package com.example.timesheetapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity{

    EditText task;
    Button search, create;
    RecyclerView list_rv;
    ListAdapter listAdapter;
    List<TimeSheet> project = new ArrayList<>();
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        task = findViewById(R.id.task);
        search = findViewById(R.id.search);
        create = findViewById(R.id.create);
        list_rv = findViewById(R.id.rv);
        swipeRefreshLayout = findViewById(R.id.swipe);

        setUpAdapter();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                searchProject();
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchProject();
            }
        });
    }

    public void createForm(View view){
        Intent i = new Intent(MainActivity.this, CreateFormActivity.class);
        startActivity(i);
    }

    public void searchProject() {
        class GetProject extends AsyncTask<Void, Void, List<TimeSheet>> {
            @Override
            protected List<TimeSheet> doInBackground(Void... voids) {
                project = DatabaseClient
                        .getInstance(getApplicationContext())
                        .getAppDatabase()
                        .timeSheetDAO()
                        .getAllProjectList();

                return project;
            }

            @Override
            protected void onPostExecute(List<TimeSheet> project) {
                super.onPostExecute(project);
                setUpAdapter();
                swipeRefreshLayout.setRefreshing(false);
            }
        }

        GetProject getProject = new GetProject();
        getProject.execute();
    }

    private void setUpAdapter() {
        listAdapter = new ListAdapter(project);
        list_rv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        list_rv.setAdapter(listAdapter);
    }
}