package com.example.timesheetapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.Serializable;
import java.util.List;

public class CreateFormActivity extends AppCompatActivity {

    EditText projectName, taskName, dateFrom, dateTo, assignee, status;
    Integer projectid = 0;
    Button save, edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_form);

        Intent intent = getIntent();
        if (intent != null){
            //Bundle bundle = intent.getExtras();
            projectid = intent.getIntExtra("projectid", projectid);
            //searchProjectFromId(projectid.getProjectId());
            Toast.makeText(this, "id: "+projectid, Toast.LENGTH_SHORT).show();
        }

        projectName = findViewById(R.id.projectName);
        taskName = findViewById(R.id.taskName);
        dateFrom = findViewById(R.id.dateFrom);
        dateTo = findViewById(R.id.dateTo);
        assignee = findViewById(R.id.assignee);
        status = findViewById(R.id.status);
        save = findViewById(R.id.save);
        edit = findViewById(R.id.edit);

        if (projectid != 0){
            save.setVisibility(View.GONE);
            edit.setVisibility(View.VISIBLE);
        }else {
            save.setVisibility(View.VISIBLE);
            edit.setVisibility(View.GONE);
        }
    }

    private void searchProjectFromId(Integer projectid) {
        class GetProject extends AsyncTask<Void, Void, List<TimeSheet>> {
            @Override
            protected List<TimeSheet> doInBackground(Void... voids) {
                DatabaseClient
                        .getInstance(getApplicationContext())
                        .getAppDatabase()
                        .timeSheetDAO()
                        .selectProjectFromId(projectid);

                return null;
            }

            @Override
            protected void onPostExecute(List<TimeSheet> tasks) {
                super.onPostExecute(tasks);
                projectName.setText(tasks.indexOf(1));
                taskName.setText(tasks.indexOf(2));
                dateFrom.setText(tasks.indexOf(3));
                dateTo.setText(tasks.indexOf(4));
                assignee.setText(tasks.indexOf(5));
                status.setText(tasks.indexOf(6));
            }
        }

        GetProject getProject = new GetProject();
        getProject.execute();
    }

    public void saveForm(View view) {
        saveToDatabase();
    }

    public void editForm(View view) {
        editToDatabase();
    }

    private void editToDatabase() {
        class EditTaskInBackend extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                TimeSheet timeSheet = new TimeSheet();
                timeSheet.setProjectName(projectName.getText().toString());
                timeSheet.setTaskName(taskName.getText().toString());
                timeSheet.setFrom(dateFrom.getText().toString());
                timeSheet.setTo(dateTo.getText().toString());
                timeSheet.setAssignedTo(assignee.getText().toString());
                timeSheet.setStatus(status.getText().toString());

                DatabaseClient.getInstance(CreateFormActivity.this)
                        .getAppDatabase()
                        .timeSheetDAO()
                        .updateProjectFromId(projectid,
                                projectName.getText().toString(),
                                taskName.getText().toString(),
                                dateFrom.getText().toString(),
                                dateTo.getText().toString(),
                                assignee.getText().toString(),
                                status.getText().toString());
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                projectName.setText("");
                taskName.setText("");
                dateFrom.setText("");
                dateTo.setText("");
                assignee.setText("");
                status.setText("");
                Toast.makeText(CreateFormActivity.this, "Item Edited", Toast.LENGTH_SHORT).show();
            }
        }
        EditTaskInBackend st = new EditTaskInBackend();
        st.execute();
    }

    private void saveToDatabase() {
        class saveTaskInBackend extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                TimeSheet timeSheet = new TimeSheet();
                timeSheet.setProjectName(projectName.getText().toString());
                timeSheet.setTaskName(taskName.getText().toString());
                timeSheet.setFrom(dateFrom.getText().toString());
                timeSheet.setTo(dateTo.getText().toString());
                timeSheet.setAssignedTo(assignee.getText().toString());
                timeSheet.setStatus(status.getText().toString());

                DatabaseClient.getInstance(CreateFormActivity.this)
                        .getAppDatabase()
                        .timeSheetDAO()
                        .addProject(timeSheet);

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                projectName.setText("");
                taskName.setText("");
                dateFrom.setText("");
                dateTo.setText("");
                assignee.setText("");
                status.setText("");
                if (projectid != null){
                    Toast.makeText(CreateFormActivity.this, "Item Edited", Toast.LENGTH_SHORT).show();

                }
                Toast.makeText(CreateFormActivity.this, "Item Created", Toast.LENGTH_SHORT).show();
            }
        }
        saveTaskInBackend st = new saveTaskInBackend();
        st.execute();
    }

    public void goBack(View view) {
        finish();
    }
}