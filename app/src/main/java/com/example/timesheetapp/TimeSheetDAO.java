package com.example.timesheetapp;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import java.util.List;

@Dao
public interface TimeSheetDAO {
    @Query("SELECT * FROM TimeSheet")
    List<TimeSheet> getAllProjectList();
    @Insert
    void addProject(TimeSheet timeSheet);
    @Query("DELETE FROM TimeSheet WHERE projectId = :projectId")
    void deleteProjectFromId(int projectId);
    @Query("SELECT * FROM TimeSheet WHERE projectId = :projectId")
    TimeSheet selectProjectFromId(int projectId);
    @Query("UPDATE TimeSheet SET projectName = :projectName, taskName = :taskName, assignedTo = :assignedTo, datefrom = :dateFrom, " +
            "dateTo = :dateTo, status = :status WHERE projectId = :projectId")
    void updateProjectFromId(int projectId, String projectName, String taskName, String assignedTo , String dateFrom, String dateTo,
                             String status);
//    @Query("SELECT * FROM TimeSheet WHERE taskName LIKE :text")
//    void loadProject(String text);
}
