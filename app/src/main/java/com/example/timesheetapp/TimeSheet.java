package com.example.timesheetapp;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class TimeSheet implements Serializable {

    @PrimaryKey(autoGenerate = true)
    int projectId;
    @ColumnInfo(name = "projectName")
    String projectName;
    @ColumnInfo(name = "taskName")
    String taskName;
    @ColumnInfo(name = "assignedTo")
    String assignedTo;
    @ColumnInfo(name = "dateFrom")
    String from;
    @ColumnInfo(name = "dateTo")
    String to;
    @ColumnInfo(name = "status")
    String status;

    public TimeSheet() {

    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
